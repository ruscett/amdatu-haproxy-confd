#!/bin/bash 

set -eo pipefail

export ETCD_PORT=${ETCD_PORT:-2379}
export HOST_IP=${HOST_IP:-172.17.42.1}
export ETCD=$HOST_IP:$ETCD_PORT

echo "bootstrapping haproxy+confd. ETCD: $ETCD"

# Loop until confd has updated the haproxy config
until confd -onetime -node $ETCD -config-file /etc/confd/conf.d/haproxy.toml; do
  echo "[haproxy-confd] waiting for confd to refresh h/etc/haproxy/haproxy.cfg"
  sleep 5
done

# Start watching ETCD to update haproxy configuration
confd -node $ETCD -config-file /etc/confd/conf.d/haproxy.toml -log-level info -interval 5